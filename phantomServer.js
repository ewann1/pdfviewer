exports.run=function(){
var phantom = require('phantom');
var http = require('http');
var url = require('url');
var fs = require('fs');
var cpt = 0;
var server = http.createServer(function(request,response){
	var query = url.parse(request.url).query;
	if(query == null){
		return;
	}
	var querySplitted = query.split("=");
	if(querySplitted[0] == "page" && querySplitted.length >= 2){
		var filename = cpt+".pdf";
		console.log(""+filename);
		phantom.create(function (ph) {
			ph.createPage(function (page) {
				page.open(querySplitted[1], function (status){
					page.paperSize = {format:'A4'}
					page.render(filename,function (err,callback){
						response.setHeader("Content-Type","application/pdf");
						fs.createReadStream(filename).pipe(response);
					});
				});			/* the page actions */
			})
		}, {
		  dnodeOpts: {
			weak: false
		  }
		});
		cpt++;		
	}
});
server.listen("8060");
	
}